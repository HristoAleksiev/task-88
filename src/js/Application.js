import EventEmitter from "eventemitter3";
import anime from "animejs";
export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.init();
    this.emit(Application.events.READY);
  }
  
  init() {

    let targetPElement = document.querySelector(".article div p")
    targetPElement.addEventListener("click", (animation) =>{
      anime({
        targets: targetPElement,
        translateX: 250,
        direction: 'alternate',
        loop: false,
        easing: 'spring(3, 80, 10, 0)'
      })
    })
  }
}
